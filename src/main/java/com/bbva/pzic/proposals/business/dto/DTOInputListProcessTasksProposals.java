package com.bbva.pzic.proposals.business.dto;

import javax.validation.constraints.Size;

public class DTOInputListProcessTasksProposals {

    /**
     * Business process identifier that serves as a link between business data and the process which belongs to. Is informed at process creation time and is the same for all the tasks inside the process.
     */
    @Size(max = 40, groups = ValidationGroup.ListProcessTasksProposals.class)
    private String businessProcessId;
    /**
     *  The Task Identifier
     */
    @Size(max = 10,groups = ValidationGroup.ListProcessTasksProposals.class)
    private String taskId;

    public String getBusinessProcessId() {
        return businessProcessId;
    }

    public void setBusinessProcessId(String businessProcessId) {
        this.businessProcessId = businessProcessId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
}
