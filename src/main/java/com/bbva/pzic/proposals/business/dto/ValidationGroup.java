package com.bbva.pzic.proposals.business.dto;

/**
 * Created on 12/04/2017.
 *
 * @author Entelgy
 */
public interface ValidationGroup {

    interface ListExternalFinancingProposals {
    }

    interface CreateExternalFinancingProposal {
    }

    interface ModifyExternalFinancingProposal {
    }

    interface ListProposalsV0 {
    }

    interface SimulateProposals {
    }

    interface CreateQuestionnairesValidateAccess {
    }

    interface CreateQuestionnairesValidateAccessEmail {
    }

    interface CreateQuestionnairesValidateAccessMobile {
    }

    interface ListProcessTasksProposals{

    }
}
