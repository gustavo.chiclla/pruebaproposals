package com.bbva.pzic.proposals.dao.apx;


import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.proposals.business.dto.DTOInputListProcessTasksProposals;
import com.bbva.pzic.proposals.dao.apx.mapper.impl.ApxListProcessTasksProposalsMapper;
import com.bbva.pzic.proposals.dao.model.ppcutge1_1.PeticionTransaccionPpcutge1_1;
import com.bbva.pzic.proposals.dao.model.ppcutge1_1.RespuestaTransaccionPpcutge1_1;
import com.bbva.pzic.proposals.facade.v0.dto.ProcessTasks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class ApxListProcessTasksProposals {


    //mapper
    @Autowired
    private ApxListProcessTasksProposalsMapper mapper;


    @Resource(name = "transaccionPpcutge1_1")
    private transient InvocadorTransaccion<PeticionTransaccionPpcutge1_1, RespuestaTransaccionPpcutge1_1> transaccion;

    public ProcessTasks perform(final DTOInputListProcessTasksProposals dtoInt) {
        PeticionTransaccionPpcutge1_1 request = mapper.mapIn(dtoInt);
        RespuestaTransaccionPpcutge1_1 response = transaccion.invocar(request);
        return mapper.mapOut(response);
    }


}
