// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.proposals.dao.model.hyt6;

import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.pzic.proposals.dao.model.hyt6.PeticionTransaccionHyt6;

privileged aspect PeticionTransaccionHyt6_Roo_JavaBean {
    
    /**
     * Sets cuerpo value
     * 
     * @param cuerpo
     * @return PeticionTransaccionHyt6
     */
    public PeticionTransaccionHyt6 PeticionTransaccionHyt6.setCuerpo(CuerpoMultiparte cuerpo) {
        this.cuerpo = cuerpo;
        return this;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return String
     */
    public String PeticionTransaccionHyt6.toString() {
        return "PeticionTransaccionHyt6 {" + 
        "}" + super.toString();
    }
    
}
