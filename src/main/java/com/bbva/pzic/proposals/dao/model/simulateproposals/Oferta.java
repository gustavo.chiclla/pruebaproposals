package com.bbva.pzic.proposals.dao.model.simulateproposals;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import java.math.BigDecimal;

/**
 * Created on 28/12/2017.
 *
 * @author Entelgy
 */
public class Oferta {

    private String valBin;
    private String codPlazo;
    private String codProducto;
    private String desProducto;
    private String codSubProducto;
    private String desSubProducto;
    private String desConfigProducto;
    private String stRiesgo;
    private String divisa;
    private String familiaProducto;
    private BigDecimal rangoMin;
    private BigDecimal rangoMax;
    private BigDecimal valTasa;
    private BigDecimal valCuotaContrato;
    private BigDecimal valCuotaAjust;
    private BigDecimal valCuotaReal;
    private BigDecimal valLimiteReal;
    private BigDecimal valLimiteAjust;
    private BigDecimal valLimiteContrato;
    private BigDecimal tasaMin;
    private BigDecimal tasaMax;
    private String campanha;
    private String codigoCentral;
    private String flujoOperativo;
    private String idPropuesta;
    private String codFlujoOpe;
    private String vlaboral;
    private String vdomiciliaria;
    private String tipplazo;
    private String codConfigProducto;
    private String codInterno;
    private String documentType;
    @DatoAuditable(omitir = true)
    private String documentNumber;
    private String codigoSegmento;
    private String indicadorRiesgo;
    private String desIndicadorRiesgo;

    public String getValBin() {
        return valBin;
    }

    public void setValBin(String valBin) {
        this.valBin = valBin;
    }

    public String getCodPlazo() {
        return codPlazo;
    }

    public void setCodPlazo(String codPlazo) {
        this.codPlazo = codPlazo;
    }

    public String getCodProducto() {
        return codProducto;
    }

    public void setCodProducto(String codProducto) {
        this.codProducto = codProducto;
    }

    public String getDesProducto() {
        return desProducto;
    }

    public void setDesProducto(String desProducto) {
        this.desProducto = desProducto;
    }

    public String getCodSubProducto() {
        return codSubProducto;
    }

    public void setCodSubProducto(String codSubProducto) {
        this.codSubProducto = codSubProducto;
    }

    public String getDesSubProducto() {
        return desSubProducto;
    }

    public void setDesSubProducto(String desSubProducto) {
        this.desSubProducto = desSubProducto;
    }

    public String getDesConfigProducto() {
        return desConfigProducto;
    }

    public void setDesConfigProducto(String desConfigProducto) {
        this.desConfigProducto = desConfigProducto;
    }

    public String getStRiesgo() {
        return stRiesgo;
    }

    public void setStRiesgo(String stRiesgo) {
        this.stRiesgo = stRiesgo;
    }

    public String getDivisa() {
        return divisa;
    }

    public void setDivisa(String divisa) {
        this.divisa = divisa;
    }

    public String getFamiliaProducto() {
        return familiaProducto;
    }

    public void setFamiliaProducto(String familiaProducto) {
        this.familiaProducto = familiaProducto;
    }

    public BigDecimal getRangoMin() {
        return rangoMin;
    }

    public void setRangoMin(BigDecimal rangoMin) {
        this.rangoMin = rangoMin;
    }

    public BigDecimal getRangoMax() {
        return rangoMax;
    }

    public void setRangoMax(BigDecimal rangoMax) {
        this.rangoMax = rangoMax;
    }

    public BigDecimal getValTasa() {
        return valTasa;
    }

    public void setValTasa(BigDecimal valTasa) {
        this.valTasa = valTasa;
    }

    public BigDecimal getValCuotaContrato() {
        return valCuotaContrato;
    }

    public void setValCuotaContrato(BigDecimal valCuotaContrato) {
        this.valCuotaContrato = valCuotaContrato;
    }

    public BigDecimal getValCuotaAjust() {
        return valCuotaAjust;
    }

    public void setValCuotaAjust(BigDecimal valCuotaAjust) {
        this.valCuotaAjust = valCuotaAjust;
    }

    public BigDecimal getValCuotaReal() {
        return valCuotaReal;
    }

    public void setValCuotaReal(BigDecimal valCuotaReal) {
        this.valCuotaReal = valCuotaReal;
    }

    public BigDecimal getValLimiteReal() {
        return valLimiteReal;
    }

    public void setValLimiteReal(BigDecimal valLimiteReal) {
        this.valLimiteReal = valLimiteReal;
    }

    public BigDecimal getValLimiteAjust() {
        return valLimiteAjust;
    }

    public void setValLimiteAjust(BigDecimal valLimiteAjust) {
        this.valLimiteAjust = valLimiteAjust;
    }

    public BigDecimal getValLimiteContrato() {
        return valLimiteContrato;
    }

    public void setValLimiteContrato(BigDecimal valLimiteContrato) {
        this.valLimiteContrato = valLimiteContrato;
    }

    public BigDecimal getTasaMin() {
        return tasaMin;
    }

    public void setTasaMin(BigDecimal tasaMin) {
        this.tasaMin = tasaMin;
    }

    public BigDecimal getTasaMax() {
        return tasaMax;
    }

    public void setTasaMax(BigDecimal tasaMax) {
        this.tasaMax = tasaMax;
    }

    public String getCampanha() {
        return campanha;
    }

    public void setCampanha(String campanha) {
        this.campanha = campanha;
    }

    public String getCodigoCentral() {
        return codigoCentral;
    }

    public void setCodigoCentral(String codigoCentral) {
        this.codigoCentral = codigoCentral;
    }

    public String getFlujoOperativo() {
        return flujoOperativo;
    }

    public void setFlujoOperativo(String flujoOperativo) {
        this.flujoOperativo = flujoOperativo;
    }

    public String getIdPropuesta() {
        return idPropuesta;
    }

    public void setIdPropuesta(String idPropuesta) {
        this.idPropuesta = idPropuesta;
    }

    public String getCodFlujoOpe() {
        return codFlujoOpe;
    }

    public void setCodFlujoOpe(String codFlujoOpe) {
        this.codFlujoOpe = codFlujoOpe;
    }

    public String getVlaboral() {
        return vlaboral;
    }

    public void setVlaboral(String vlaboral) {
        this.vlaboral = vlaboral;
    }

    public String getVdomiciliaria() {
        return vdomiciliaria;
    }

    public void setVdomiciliaria(String vdomiciliaria) {
        this.vdomiciliaria = vdomiciliaria;
    }

    public String getTipplazo() {
        return tipplazo;
    }

    public void setTipplazo(String tipplazo) {
        this.tipplazo = tipplazo;
    }

    public String getCodConfigProducto() {
        return codConfigProducto;
    }

    public void setCodConfigProducto(String codConfigProducto) {
        this.codConfigProducto = codConfigProducto;
    }

    public String getCodInterno() {
        return codInterno;
    }

    public void setCodInterno(String codInterno) {
        this.codInterno = codInterno;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getCodigoSegmento() {
        return codigoSegmento;
    }

    public void setCodigoSegmento(String codigoSegmento) {
        this.codigoSegmento = codigoSegmento;
    }

    public String getIndicadorRiesgo() {
        return indicadorRiesgo;
    }

    public void setIndicadorRiesgo(String indicadorRiesgo) {
        this.indicadorRiesgo = indicadorRiesgo;
    }

    public String getDesIndicadorRiesgo() {
        return desIndicadorRiesgo;
    }

    public void setDesIndicadorRiesgo(String desIndicadorRiesgo) {
        this.desIndicadorRiesgo = desIndicadorRiesgo;
    }
}
