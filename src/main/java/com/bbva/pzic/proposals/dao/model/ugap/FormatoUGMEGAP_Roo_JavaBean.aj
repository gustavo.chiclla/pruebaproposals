// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.pzic.proposals.dao.model.ugap;

import com.bbva.pzic.proposals.dao.model.ugap.FormatoUGMEGAP;
import java.math.BigDecimal;

privileged aspect FormatoUGMEGAP_Roo_JavaBean {
    
    /**
     * Gets tipdocu value
     * 
     * @return String
     */
    public String FormatoUGMEGAP.getTipdocu() {
        return this.tipdocu;
    }
    
    /**
     * Sets tipdocu value
     * 
     * @param tipdocu
     * @return FormatoUGMEGAP
     */
    public FormatoUGMEGAP FormatoUGMEGAP.setTipdocu(String tipdocu) {
        this.tipdocu = tipdocu;
        return this;
    }
    
    /**
     * Gets nrodocu value
     * 
     * @return String
     */
    public String FormatoUGMEGAP.getNrodocu() {
        return this.nrodocu;
    }
    
    /**
     * Sets nrodocu value
     * 
     * @param nrodocu
     * @return FormatoUGMEGAP
     */
    public FormatoUGMEGAP FormatoUGMEGAP.setNrodocu(String nrodocu) {
        this.nrodocu = nrodocu;
        return this;
    }
    
    /**
     * Gets tarifa value
     * 
     * @return String
     */
    public String FormatoUGMEGAP.getTarifa() {
        return this.tarifa;
    }
    
    /**
     * Sets tarifa value
     * 
     * @param tarifa
     * @return FormatoUGMEGAP
     */
    public FormatoUGMEGAP FormatoUGMEGAP.setTarifa(String tarifa) {
        this.tarifa = tarifa;
        return this;
    }
    
    /**
     * Gets impfina value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoUGMEGAP.getImpfina() {
        return this.impfina;
    }
    
    /**
     * Sets impfina value
     * 
     * @param impfina
     * @return FormatoUGMEGAP
     */
    public FormatoUGMEGAP FormatoUGMEGAP.setImpfina(BigDecimal impfina) {
        this.impfina = impfina;
        return this;
    }
    
    /**
     * Gets diafact value
     * 
     * @return Integer
     */
    public Integer FormatoUGMEGAP.getDiafact() {
        return this.diafact;
    }
    
    /**
     * Sets diafact value
     * 
     * @param diafact
     * @return FormatoUGMEGAP
     */
    public FormatoUGMEGAP FormatoUGMEGAP.setDiafact(Integer diafact) {
        this.diafact = diafact;
        return this;
    }
    
    /**
     * Gets diapago value
     * 
     * @return Integer
     */
    public Integer FormatoUGMEGAP.getDiapago() {
        return this.diapago;
    }
    
    /**
     * Sets diapago value
     * 
     * @param diapago
     * @return FormatoUGMEGAP
     */
    public FormatoUGMEGAP FormatoUGMEGAP.setDiapago(Integer diapago) {
        this.diapago = diapago;
        return this;
    }
    
    /**
     * Gets mailcon value
     * 
     * @return String
     */
    public String FormatoUGMEGAP.getMailcon() {
        return this.mailcon;
    }
    
    /**
     * Sets mailcon value
     * 
     * @param mailcon
     * @return FormatoUGMEGAP
     */
    public FormatoUGMEGAP FormatoUGMEGAP.setMailcon(String mailcon) {
        this.mailcon = mailcon;
        return this;
    }
    
    /**
     * Gets tipenvi value
     * 
     * @return String
     */
    public String FormatoUGMEGAP.getTipenvi() {
        return this.tipenvi;
    }
    
    /**
     * Sets tipenvi value
     * 
     * @param tipenvi
     * @return FormatoUGMEGAP
     */
    public FormatoUGMEGAP FormatoUGMEGAP.setTipenvi(String tipenvi) {
        this.tipenvi = tipenvi;
        return this;
    }
    
    /**
     * Gets moneda value
     * 
     * @return String
     */
    public String FormatoUGMEGAP.getMoneda() {
        return this.moneda;
    }
    
    /**
     * Sets moneda value
     * 
     * @param moneda
     * @return FormatoUGMEGAP
     */
    public FormatoUGMEGAP FormatoUGMEGAP.setMoneda(String moneda) {
        this.moneda = moneda;
        return this;
    }
    
    /**
     * Gets idtoken value
     * 
     * @return String
     */
    public String FormatoUGMEGAP.getIdtoken() {
        return this.idtoken;
    }
    
    /**
     * Sets idtoken value
     * 
     * @param idtoken
     * @return FormatoUGMEGAP
     */
    public FormatoUGMEGAP FormatoUGMEGAP.setIdtoken(String idtoken) {
        this.idtoken = idtoken;
        return this;
    }
    
    /**
     * Gets impbien value
     * 
     * @return BigDecimal
     */
    public BigDecimal FormatoUGMEGAP.getImpbien() {
        return this.impbien;
    }
    
    /**
     * Sets impbien value
     * 
     * @param impbien
     * @return FormatoUGMEGAP
     */
    public FormatoUGMEGAP FormatoUGMEGAP.setImpbien(BigDecimal impbien) {
        this.impbien = impbien;
        return this;
    }
    
    /**
     * Gets codbien value
     * 
     * @return String
     */
    public String FormatoUGMEGAP.getCodbien() {
        return this.codbien;
    }
    
    /**
     * Sets codbien value
     * 
     * @param codbien
     * @return FormatoUGMEGAP
     */
    public FormatoUGMEGAP FormatoUGMEGAP.setCodbien(String codbien) {
        this.codbien = codbien;
        return this;
    }
    
    /**
     * Gets codtr value
     * 
     * @return String
     */
    public String FormatoUGMEGAP.getCodtr() {
        return this.codtr;
    }
    
    /**
     * Sets codtr value
     * 
     * @param codtr
     * @return FormatoUGMEGAP
     */
    public FormatoUGMEGAP FormatoUGMEGAP.setCodtr(String codtr) {
        this.codtr = codtr;
        return this;
    }
    
    /**
     * Gets codcnc value
     * 
     * @return String
     */
    public String FormatoUGMEGAP.getCodcnc() {
        return this.codcnc;
    }
    
    /**
     * Sets codcnc value
     * 
     * @param codcnc
     * @return FormatoUGMEGAP
     */
    public FormatoUGMEGAP FormatoUGMEGAP.setCodcnc(String codcnc) {
        this.codcnc = codcnc;
        return this;
    }
    
    /**
     * Gets codemp value
     * 
     * @return String
     */
    public String FormatoUGMEGAP.getCodemp() {
        return this.codemp;
    }
    
    /**
     * Sets codemp value
     * 
     * @param codemp
     * @return FormatoUGMEGAP
     */
    public FormatoUGMEGAP FormatoUGMEGAP.setCodemp(String codemp) {
        this.codemp = codemp;
        return this;
    }
    
    /**
     * Gets cocliex value
     * 
     * @return String
     */
    public String FormatoUGMEGAP.getCocliex() {
        return this.cocliex;
    }
    
    /**
     * Sets cocliex value
     * 
     * @param cocliex
     * @return FormatoUGMEGAP
     */
    public FormatoUGMEGAP FormatoUGMEGAP.setCocliex(String cocliex) {
        this.cocliex = cocliex;
        return this;
    }
    
    /**
     * Gets codofic value
     * 
     * @return String
     */
    public String FormatoUGMEGAP.getCodofic() {
        return this.codofic;
    }
    
    /**
     * Sets codofic value
     * 
     * @param codofic
     * @return FormatoUGMEGAP
     */
    public FormatoUGMEGAP FormatoUGMEGAP.setCodofic(String codofic) {
        this.codofic = codofic;
        return this;
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @return String
     */
    public String FormatoUGMEGAP.toString() {
        return "FormatoUGMEGAP {" + 
                "tipdocu='" + tipdocu + '\'' + 
                ", nrodocu='" + nrodocu + '\'' + 
                ", tarifa='" + tarifa + '\'' + 
                ", impfina='" + impfina + '\'' + 
                ", diafact='" + diafact + '\'' + 
                ", diapago='" + diapago + '\'' + 
                ", mailcon='" + mailcon + '\'' + 
                ", tipenvi='" + tipenvi + '\'' + 
                ", moneda='" + moneda + '\'' + 
                ", idtoken='" + idtoken + '\'' + 
                ", impbien='" + impbien + '\'' + 
                ", codbien='" + codbien + '\'' + 
                ", codtr='" + codtr + '\'' + 
                ", codcnc='" + codcnc + '\'' + 
                ", codemp='" + codemp + '\'' + 
                ", cocliex='" + cocliex + '\'' + 
                ", codofic='" + codofic + '\'' + "}" + super.toString();
    }
    
}
