package com.bbva.pzic.proposals.dao.model.simulateproposals;

/**
 * Created on 28/12/2017.
 *
 * @author Entelgy
 */
public class ProductClassification {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
