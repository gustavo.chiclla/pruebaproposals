package com.bbva.pzic.proposals.dao.apx.mapper.impl;

import com.bbva.pzic.proposals.business.dto.DTOInputListProcessTasksProposals;
import com.bbva.pzic.proposals.dao.apx.mapper.IApxListProcessTasksProposalsMapper;
import com.bbva.pzic.proposals.dao.model.ppcutge1_1.PeticionTransaccionPpcutge1_1;
import com.bbva.pzic.proposals.dao.model.ppcutge1_1.RespuestaTransaccionPpcutge1_1;
import com.bbva.pzic.proposals.facade.v0.dto.ProcessTasks;
import com.bbva.pzic.proposals.facade.v0.dto.Status;
import org.springframework.stereotype.Component;

@Component
public class ApxListProcessTasksProposalsMapper implements IApxListProcessTasksProposalsMapper {

    @Override
    public PeticionTransaccionPpcutge1_1 mapIn(final DTOInputListProcessTasksProposals dtoInt) {
        PeticionTransaccionPpcutge1_1 objPpcutge1_1 = new PeticionTransaccionPpcutge1_1();
        objPpcutge1_1.setBusinessprocessid(dtoInt.getBusinessProcessId());
        objPpcutge1_1.setTaskid(dtoInt.getTaskId());
        return objPpcutge1_1;
    }

    @Override
    public ProcessTasks mapOut(RespuestaTransaccionPpcutge1_1 respuestaTransaccionPpcutge1_1) {
        if(respuestaTransaccionPpcutge1_1 ==  null){
            return null;
        }
        ProcessTasks processTasks = new ProcessTasks();
        processTasks.setBusinessProcessId(respuestaTransaccionPpcutge1_1.getBusinessprocessid());
        processTasks.setTaskId(respuestaTransaccionPpcutge1_1.getTaskid());
        processTasks.setStatus(mapInStatus(respuestaTransaccionPpcutge1_1.getStatus()));
        return processTasks;
    }

    private Status mapInStatus(final com.bbva.pzic.proposals.dao.model.ppcutge1_1.Status status) {
        if(status == null){
            return null;
        }
        if(status.getId() == null && status.getDescription() == null){
           return null;
        }

        Status s = new Status();
        s.setId(status.getId());
        s.setDescription(status.getDescription());
        return s;
    }
}
