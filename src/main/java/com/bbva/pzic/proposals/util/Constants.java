package com.bbva.pzic.proposals.util;

/**
 * @author Entelgy
 */
public final class Constants {

    public static final String BCS_OPERATION_TRACER = "BCS-Operation-Tracer";

    public static final String CUSTOMER_ID = "customerId";
    public static final String DOCUMENT_TYPE = "documentType";
    public static final String DOCUMENT_NUMBER = "documentNumber";
    public static final String PRODUCT_CLASSIFICATION_ID = "product.productClassification.id";
    public static final String PAGINATION_KEY = "paginationKey";
    public static final String PAGE_SIZE = "pageSize";

    public static final String THIRD_PARTY_PROVIDER_ID = "thirdPartyProvider.id";
    public static final String EXTERNAL_PRODUCT_CATEGORY_TYPE_ID = "externalProduct.category.categoryType.id";
    public static final String HOLDER_IDENTITY_DOCUMENTS_DOCUMENT_TYPE_ID = "holder.identityDocuments.documentType.id";
    public static final String HOLDER_IDENTITY_DOCUMENTS_DOCUMENT_NUMBER = "holder.identityDocuments.documentNumber";
    public static final String FROM_REQUEST_DATE = "fromRequestDate";
    public static final String TO_REQUEST_DATE = "toRequestDate";

    private Constants() {
    }
}
