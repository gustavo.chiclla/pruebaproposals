package com.bbva.pzic.proposals.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.proposals.business.dto.DTOInputListProcessTasksProposals;
import com.bbva.pzic.proposals.facade.v0.dto.ProcessTasks;
import com.bbva.pzic.proposals.facade.v0.mapper.IListProcessTasksProposalsMapper;
import org.springframework.stereotype.Component;


@Component
public class ListProcessTasksProposalsMapper implements IListProcessTasksProposalsMapper {

    @Override
    public DTOInputListProcessTasksProposals mapIn(final String businessProcessId, final String taskId) {

        DTOInputListProcessTasksProposals inputListProcessTasksProposals  = new DTOInputListProcessTasksProposals();
        inputListProcessTasksProposals.setBusinessProcessId(businessProcessId);
        inputListProcessTasksProposals.setTaskId(taskId);
        return inputListProcessTasksProposals;
    }

    @Override
    public ServiceResponse<ProcessTasks> mapOut(final ProcessTasks processTasks) {
        return ServiceResponse.data(processTasks).pagination(null).build();
    }
}
