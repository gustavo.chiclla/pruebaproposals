package com.bbva.pzic.proposals.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.proposals.business.dto.DTOInputListProcessTasksProposals;
import com.bbva.pzic.proposals.dao.model.ppcutge1_1.PeticionTransaccionPpcutge1_1;
import com.bbva.pzic.proposals.dao.model.ppcutge1_1.RespuestaTransaccionPpcutge1_1;
import com.bbva.pzic.proposals.facade.v0.dto.ProcessTasks;

import java.util.List;

public interface IListProcessTasksProposalsMapper {


    //Return to DTO to use it in facade/impl/SrvProposalsV0
    DTOInputListProcessTasksProposals mapIn(String businessProcessId, String taskId);

    //Return ServiceResponse to use it in facade/impl/SrvProposalsV0
    ServiceResponse<ProcessTasks> mapOut(ProcessTasks listProcessTasks);



}
