package com.bbva.pzic.proposals.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "processTasks", namespace = "urn:com:bbva:pzic:proposals:facade:v0:dto")
@XmlType(name = "processTasks", namespace = "urn:com:bbva:pzic:proposals:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProcessTasks implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Business process identifier that serves as a link between business data and the process which belongs to. Is informed at process creation time and is the same for all the tasks inside the process.
     */
    private String businessProcessId;

    /**
     * The Task Identifier.
     */
    private String taskId;

    /**
     * Information about the status of the task.
     */
    private Status status;

    public String getBusinessProcessId() {
        return businessProcessId;
    }

    public void setBusinessProcessId(String businessProcessId) {
        this.businessProcessId = businessProcessId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
