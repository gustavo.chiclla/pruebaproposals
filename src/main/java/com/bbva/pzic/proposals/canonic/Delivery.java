package com.bbva.pzic.proposals.canonic;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 10/05/2017.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "Delivery", namespace = "urn:com:bbva:pzic:proposals:canonic")
@XmlType(name = "Delivery", namespace = "urn:com:bbva:pzic:proposals:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Delivery implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Type of delivery of documentation.
     */
    private DeliveryType deliveryType;
    /**
     * Email where the documentation is delivered.
     */
    @DatoAuditable(omitir = true)
    private String email;

    public DeliveryType getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(DeliveryType deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}