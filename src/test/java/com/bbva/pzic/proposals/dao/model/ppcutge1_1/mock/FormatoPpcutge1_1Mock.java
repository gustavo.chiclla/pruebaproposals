package com.bbva.pzic.proposals.dao.model.ppcutge1_1.mock;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.proposals.dao.model.ppcutge1_1.PeticionTransaccionPpcutge1_1;
import com.bbva.pzic.proposals.dao.model.ppcutge1_1.RespuestaTransaccionPpcutge1_1;
import com.bbva.pzic.proposals.facade.v0.dto.ProcessTasks;
import com.bbva.pzic.proposals.util.helper.ObjectMapperHelper;

import java.io.IOException;

public class FormatoPpcutge1_1Mock {

    private final ObjectMapperHelper mapper = ObjectMapperHelper.getInstance();

    private static final FormatoPpcutge1_1Mock INSTANCE = new FormatoPpcutge1_1Mock();

    public static FormatoPpcutge1_1Mock getInstance() { return INSTANCE; }

    public FormatoPpcutge1_1Mock() {
    }

    public PeticionTransaccionPpcutge1_1 getPeticionPpcutge1_1(){

        try {
            return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                    "com/bbva/pzic/proposals/dao/mock/peticionTransaccionPpcutge1_1.json"), PeticionTransaccionPpcutge1_1.class);
        } catch (Exception e) {
           throw new BusinessServiceException("technicalError", e);
        }

    }

    public RespuestaTransaccionPpcutge1_1 getRespuestaPpcutge1_1(){
        try {
            return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                    "com/bbva/pzic/proposals/dao/mock/respuestaTransaccionPpcutge1_1.json"), RespuestaTransaccionPpcutge1_1.class);
        } catch (IOException e) {
            throw new BusinessServiceException("technicalError", e);
        }
    }



}
