package com.bbva.pzic.proposals.dao.apx.mapper.impl;

import com.bbva.pzic.proposals.EntityStubs;
import com.bbva.pzic.proposals.business.dto.DTOInputListProcessTasksProposals;
import com.bbva.pzic.proposals.dao.model.ppcutge1_1.PeticionTransaccionPpcutge1_1;
import com.bbva.pzic.proposals.dao.model.ppcutge1_1.RespuestaTransaccionPpcutge1_1;
import com.bbva.pzic.proposals.dao.model.ppcutge1_1.mock.FormatoPpcutge1_1Mock;
import com.bbva.pzic.proposals.facade.v0.dto.ProcessTasks;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ApxListProcessTasksProposalsMapperTest {

    private ApxListProcessTasksProposalsMapper mapper;

    @Before
    public void setUp() {mapper = new ApxListProcessTasksProposalsMapper();}

    @Test
    public void mapInFullTest() throws IOException {
        DTOInputListProcessTasksProposals input = EntityStubs.getInstance().getDTOInputListProcessTasksProposals();
        PeticionTransaccionPpcutge1_1 result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getBusinessprocessid());
        assertNotNull(result.getTaskid());
    }

    @Test
    public void mapInEmptyTest() throws IOException {
        DTOInputListProcessTasksProposals input = new DTOInputListProcessTasksProposals();
        PeticionTransaccionPpcutge1_1 result = mapper.mapIn(input);
        assertNotNull(result);
        assertNull(result.getBusinessprocessid());
        assertNull(result.getTaskid());
    }

    @Test
    public void mapOutRespuestaTransaccionPpcutge1_1(){
        RespuestaTransaccionPpcutge1_1 input = FormatoPpcutge1_1Mock.getInstance().getRespuestaPpcutge1_1();
        ProcessTasks resp = mapper.mapOut(input);
        assertNotNull(resp);
        assertNotNull(resp.getTaskId());
        assertNotNull(resp.getBusinessProcessId());
        assertNotNull(resp.getStatus());
        assertNotNull(resp.getStatus().getId());
        assertNotNull(resp.getStatus().getDescription());

        assertEquals(input.getBusinessprocessid(), resp.getBusinessProcessId());
        assertEquals(input.getTaskid(), resp.getTaskId());

        assertEquals(input.getStatus().getId(), resp.getStatus().getId());
        assertEquals(input.getStatus().getDescription(), resp.getStatus().getDescription());
    }

}
